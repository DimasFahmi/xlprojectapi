package com.xl.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RestController {    

	@RequestMapping("/Percobaan")
	@ResponseBody
    public String getHome() {
        return "Dimas Fahmi Suntoro";
    }

}